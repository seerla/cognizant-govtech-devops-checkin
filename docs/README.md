# GovTech DevOps Technical Assignment Execution Summary


This is a README file to walk through on 4 assigned tasks implementation

## Table of Contents


- [Pre-requisites](#pre-requisites)
- [Containerisation](#containerisation)
- [Environment](#environment)
- [Docker-Compose](#docker-compose)
- [Pipelines](#pipelines)
- [Evidence](#evidence)

## Pre-requisites

1. Clone the repository from default main branch
2. Install the required docker and docker-compose on testing machine
3. Access to GitLab portal for read only access and access to Docker hub to download docker images

## Containerisation

# TASK1

1. Followed a multi-stage docker build approach to have only running binary on final docker image
2. Created final image with 10.8 MB to run pinger application
3. created 3 test cases to have test driven approach with below pre-requisties before final docker image created
   1. go version should be higher than 1.20
   2. all go dependency modules are downloaded and verifies
   3. pinger binary successfully created in /bin/pinger
      Use the `docker build -f ./deployments/build/Dockerfile -t devops/pinger:latest --progress=plain .` for getting docker test cases output
4. Test cases steps will add an additional docker layers, however early test and fail will be more value to team and business.

Dockerfile without testcases

```
# Building with latest golang image
#Using Docker Multistage Build strategy to have final image with executable binary


# Created a 3 test cases with exit condition to validate all pre-requisties before creating a final docker image

FROM golang:alpine as builder
WORKDIR /go/src/pinger

COPY go.mod go.sum ./
COPY ./cmd/pinger .

RUN go mod vendor
RUN CGO_ENABLED=0 GOOS=linux go build -mod vendor -a -installsuffix cgo -o pinger .
FROM istio/distroless
WORKDIR /app
COPY --from=builder /go/src/pinger/pinger /app/
ENV PATH=$PATH:/app/
ENTRYPOINT ["pinger"]

```

5. output of `make docker_image`
```
make docker_image
# DO NOT CHANGE THIS
docker build -f ./deployments/build/Dockerfile -t devops/pinger:latest .
[+] Building 2.6s (18/18) FINISHED                                                                                                                                                                                            docker:desktop-linux
 => [internal] load build definition from Dockerfile                                                                                                                                                                                          0.0s
 => => transferring dockerfile: 1.46kB                                                                                                                                                                                                        0.0s
 => [internal] load metadata for docker.io/istio/distroless:latest                                                                                                                                                                            2.1s
 => [internal] load metadata for docker.io/library/golang:alpine                                                                                                                                                                              2.6s
 => [internal] load .dockerignore                                                                                                                                                                                                             0.0s
 => => transferring context: 2B                                                                                                                                                                                                               0.0s
 => [builder 1/9] FROM docker.io/library/golang:alpine@sha256:cdc86d9f363e8786845bea2040312b4efa321b828acdeb26f393faa864d887b0                                                                                                                0.0s
 => [internal] load build context                                                                                                                                                                                                             0.0s
 => => transferring context: 267B                                                                                                                                                                                                             0.0s
 => [stage-1 1/3] FROM docker.io/istio/distroless:latest@sha256:9d619eb66f60615eddd1ebe8a8a1b211dedbc00bcb84fff79560a4994a6312d0                                                                                                              0.0s
 => CACHED [stage-1 2/3] WORKDIR /app                                                                                                                                                                                                         0.0s
 => CACHED [builder 2/9] WORKDIR /go/src/pinger                                                                                                                                                                                               0.0s
 => CACHED [builder 3/9] COPY go.mod go.sum ./                                                                                                                                                                                                0.0s
 => CACHED [builder 4/9] COPY ./cmd/pinger .                                                                                                                                                                                                  0.0s
 => CACHED [builder 5/9] RUN go_version=$(go version | awk '{print $3}' | sed 's/go//g') &&     if [ "$go_version" '>' "1.20" ]; then         echo "Go version is greater than 1.20";     else         echo "Go version is not greater than   0.0s
 => CACHED [builder 6/9] RUN go mod vendor                                                                                                                                                                                                    0.0s
 => CACHED [builder 7/9] RUN go_mod_verify=$(go mod verify) && if [ "$go_mod_verify" = "all modules verified" ]; then         echo "All go modules are verified";     else         echo "Failed to verify all modules" && exit 1;     fi      0.0s
 => CACHED [builder 8/9] RUN CGO_ENABLED=0 GOOS=linux go build -mod vendor -a -installsuffix cgo -o pinger .                                                                                                                                  0.0s
 => CACHED [builder 9/9] RUN test -e ./pinger &&     if [ $? -eq 0 ]; then      echo "pinger binary is created successfully";     else      echo "pinger binary failed to create " && exit 1;    fi                                           0.0s
 => CACHED [stage-1 3/3] COPY --from=builder /go/src/pinger/pinger /app/                                                                                                                                                                      0.0s
 => exporting to image                                                                                                                                                                                                                        0.0s
 => => exporting layers                                                                                                                                                                                                                       0.0s
 => => writing image sha256:4b959121fb1ce4e7f25cbe51856d19ad44329a9fc58d8dcd11e2dedd5393a6c5                                                                                                                                                  0.0s
 => => naming to docker.io/devops/pinger:latest                                                                                                                                                                                               0.0s

View build details: docker-desktop://dashboard/build/desktop-linux/desktop-linux/6meurhv56jf3i43406emies92


```
6. output of `make docker_testrun`

```
# DO NOT CHANGE THIS
docker run -it -p 8000:8000 devops/pinger:latest
service|2024/04/17 08:08:05 initialising service...
service|2024/04/17 08:08:05 attempting to listen on '0.0.0.0:8000'...
 server|2024/04/17 08:08:06 < localhost:8000 <- 127.0.0.1:36526 | HTTP/1.1 GET / 
service|2024/04/17 08:08:06 > http://localhost:8000/ -> '200 OK'
 server|2024/04/17 08:08:07 < localhost:8000 <- 127.0.0.1:36534 | HTTP/1.1 GET / 
service|2024/04/17 08:08:07 > http://localhost:8000/ -> '200 OK'
 server|2024/04/17 08:08:08 < localhost:8000 <- 127.0.0.1:36536 | HTTP/1.1 GET / 
service|2024/04/17 08:08:08 > http://localhost:8000/ -> '200 OK'
 server|2024/04/17 08:08:09 < localhost:8000 <- 127.0.0.1:36544 | HTTP/1.1 GET / 
service|2024/04/17 08:08:09 > http://localhost:8000/ -> '200 OK'
 server|2024/04/17 08:08:10 < localhost:8000 <- 127.0.0.1:36548 | HTTP/1.1 GET / 
service|2024/04/17 08:08:10 > http://localhost:8000/ -> '200 OK'
 server|2024/04/17 08:08:11 < localhost:8000 <- 127.0.0.1:36564 | HTTP/1.1 GET / 
service|2024/04/17 08:08:11 > http://localhost:8000/ -> '200 OK'
 server|2024/04/17 08:08:12 < localhost:8000 <- 127.0.0.1:43930 | HTTP/1.1 GET / 
service|2024/04/17 08:08:12 > http://localhost:8000/ -> '200 OK'
 server|2024/04/17 08:08:13 < localhost:8000 <- 127.0.0.1:43932 | HTTP/1.1 GET / 
service|2024/04/17 08:08:13 > http://localhost:8000/ -> '200 OK'
 server|2024/04/17 08:08:14 < localhost:8000 <- 127.0.0.1:43944 | HTTP/1.1 GET / 
service|2024/04/17 08:08:14 > http://localhost:8000/ -> '200 OK'
 server|2024/04/17 08:08:15 < localhost:8000 <- 127.0.0.1:43952 | HTTP/1.1 GET / 
service|2024/04/17 08:08:15 > http://localhost:8000/ -> '200 OK'
 server|2024/04/17 08:08:16 < localhost:8000 <- 127.0.0.1:43962 | HTTP/1.1 GET / 
service|2024/04/17 08:08:16 > http://localhost:8000/ -> '200 OK'
 server|2024/04/17 08:08:17 < localhost:8000 <- 127.0.0.1:43978 | HTTP/1.1 GET / 
service|2024/04/17 08:08:17 > http://localhost:8000/ -> '200 OK'
 server|2024/04/17 08:08:18 < localhost:8000 <- 127.0.0.1:43980 | HTTP/1.1 GET / 
service|2024/04/17 08:08:18 > http://localhost:8000/ -> '200 OK'
```
## Environment

# TASK2

Created a docker-compose.yml to have below capabilities

1. Two services named as `pingerhost1` and `pingerhost2`
2. Updated pinger service on `pingerhost1` to run on port 9999
3. Updated `pingerhost1` service use TARGET_HOST as `pingerhost2`
4. Updated `pingerhost2` service to use TARGET_HOST as `pingerhost1`
5. Created a dedicated private network `pinger` for docker containers communication through dedicated docker network
6. Here is a docker-compose.yml
```
version: "3"
services:
  pingerhost1:
    image: devops/pinger:latest 
    environment:
      PORT: 9999 
      TARGET_HOST: pingerhost2
    networks:
      - pinger
  pingerhost2:
    image: devops/pinger:latest
    environment:
      TARGET_HOST: pingerhost1
      TARGET_PORT: 9999 
    networks:
      - pinger
networks:
 pinger: {}

```

## Docker-Compose

# TASK3

Run below command to start docker compose services up
`docker-compose -f ./deployments/docker-compose.yml up -V`

Expected output from above command will demonstrate as follows

1. Successfully created a new docker network called `deployments_pinger`
2. Created docker services `pingerhost1` and `pingerhost2`
3. `pingerhost1` is running on port 9999 and connecting to `pingerhost2` on default port `8000`
4. `pingerhost2` is running on default port `8000` and connecting to `pingerhost1` on port `9999`
5. Expected output as below

```
# DO NOT CHANGE THIS
docker-compose -f ./deployments/docker-compose.yml up -V
[+] Running 2/0
 ✔ Container deployments-pingerhost1-1  Created                                                                                                                                                                                               0.0s 
 ✔ Container deployments-pingerhost2-1  Created                                                                                                                                                                                               0.0s 
Attaching to pingerhost1-1, pingerhost2-1
pingerhost1-1  | service|2024/04/17 08:09:25 initialising service...
pingerhost1-1  | service|2024/04/17 08:09:25 attempting to listen on '0.0.0.0:9999'...
pingerhost2-1  | service|2024/04/17 08:09:25 initialising service...
pingerhost2-1  | service|2024/04/17 08:09:25 attempting to listen on '0.0.0.0:8000'...
pingerhost1-1  |  server|2024/04/17 08:09:26 < pingerhost1:9999 <- 172.18.0.2:57406 | HTTP/1.1 GET / 
pingerhost2-1  |  server|2024/04/17 08:09:26 < pingerhost2:8000 <- 172.18.0.3:48796 | HTTP/1.1 GET / 
pingerhost2-1  | service|2024/04/17 08:09:26 > http://pingerhost1:9999/ -> '200 OK'
pingerhost1-1  | service|2024/04/17 08:09:26 > http://pingerhost2:8000/ -> '200 OK'
pingerhost1-1  |  server|2024/04/17 08:09:27 < pingerhost1:9999 <- 172.18.0.2:57416 | HTTP/1.1 GET / 
pingerhost1-1  | service|2024/04/17 08:09:27 > http://pingerhost2:8000/ -> '200 OK'
pingerhost2-1  |  server|2024/04/17 08:09:27 < pingerhost2:8000 <- 172.18.0.3:48812 | HTTP/1.1 GET / 
pingerhost2-1  | service|2024/04/17 08:09:27 > http://pingerhost1:9999/ -> '200 OK'
pingerhost2-1  |  server|2024/04/17 08:09:28 < pingerhost2:8000 <- 172.18.0.3:48816 | HTTP/1.1 GET / 
pingerhost2-1  | service|2024/04/17 08:09:28 > http://pingerhost1:9999/ -> '200 OK'
pingerhost1-1  |  server|2024/04/17 08:09:28 < pingerhost1:9999 <- 172.18.0.2:57432 | HTTP/1.1 GET / 
pingerhost1-1  | service|2024/04/17 08:09:28 > http://pingerhost2:8000/ -> '200 OK'
pingerhost2-1  |  server|2024/04/17 08:09:29 < pingerhost2:8000 <- 172.18.0.3:48822 | HTTP/1.1 GET / 
pingerhost2-1  | service|2024/04/17 08:09:29 > http://pingerhost1:9999/ -> '200 OK'
pingerhost1-1  |  server|2024/04/17 08:09:29 < pingerhost1:9999 <- 172.18.0.2:57436 | HTTP/1.1 GET / 
pingerhost1-1  | service|2024/04/17 08:09:29 > http://pingerhost2:8000/ -> '200 OK'
pingerhost2-1  |  server|2024/04/17 08:09:30 < pingerhost2:8000 <- 172.18.0.3:48834 | HTTP/1.1 GET / 
pingerhost2-1  | service|2024/04/17 08:09:30 > http://pingerhost1:9999/ -> '200 OK'
pingerhost1-1  | service|2024/04/17 08:09:30 > http://pingerhost2:8000/ -> '200 OK'
pingerhost1-1  |  server|2024/04/17 08:09:30 < pingerhost1:9999 <- 172.18.0.2:57452 | HTTP/1.1 GET / 
pingerhost2-1  |  server|2024/04/17 08:09:31 < pingerhost2:8000 <- 172.18.0.3:48846 | HTTP/1.1 GET / 
pingerhost2-1  | service|2024/04/17 08:09:31 > http://pingerhost1:9999/ -> '200 OK'
pingerhost1-1  |  server|2024/04/17 08:09:31 < pingerhost1:9999 <- 172.18.0.2:57454 | HTTP/1.1 GET / 
pingerhost1-1  | service|2024/04/17 08:09:31 > http://pingerhost2:8000/ -> '200 OK'
pingerhost1-1  |  server|2024/04/17 08:09:32 < pingerhost1:9999 <- 172.18.0.2:51454 | HTTP/1.1 GET / 
pingerhost1-1  | service|2024/04/17 08:09:32 > http://pingerhost2:8000/ -> '200 OK'
pingerhost2-1  |  server|2024/04/17 08:09:32 < pingerhost2:8000 <- 172.18.0.3:40942 | HTTP/1.1 GET / 
pingerhost2-1  | service|2024/04/17 08:09:32 > http://pingerhost1:9999/ -> '200 OK'
pingerhost2-1  |  server|2024/04/17 08:09:33 < pingerhost2:8000 <- 172.18.0.3:40944 | HTTP/1.1 GET / 
pingerhost2-1  | service|2024/04/17 08:09:33 > http://pingerhost1:9999/ -> '200 OK'
pingerhost1-1  |  server|2024/04/17 08:09:33 < pingerhost1:9999 <- 172.18.0.2:51470 | HTTP/1.1 GET / 
pingerhost1-1  | service|2024/04/17 08:09:33 > http://pingerhost2:8000/ -> '200 OK'
pingerhost1-1  |  server|2024/04/17 08:09:34 < pingerhost1:9999 <- 172.18.0.2:51480 | HTTP/1.1 GET / 
pingerhost1-1  | service|2024/04/17 08:09:34 > http://pingerhost2:8000/ -> '200 OK'
pingerhost2-1  |  server|2024/04/17 08:09:34 < pingerhost2:8000 <- 172.18.0.3:40956 | HTTP/1.1 GET / 
pingerhost2-1  | service|2024/04/17 08:09:34 > http://pingerhost1:9999/ -> '200 OK'
pingerhost1-1  |  server|2024/04/17 08:09:35 < pingerhost1:9999 <- 172.18.0.2:51488 | HTTP/1.1 GET / 
pingerhost1-1  | service|2024/04/17 08:09:35 > http://pingerhost2:8000/ -> '200 OK'
pingerhost2-1  |  server|2024/04/17 08:09:35 < pingerhost2:8000 <- 172.18.0.3:40972 | HTTP/1.1 GET / 
pingerhost2-1  | service|2024/04/17 08:09:35 > http://pingerhost1:9999/ -> '200 OK'

```

```
docker network ls | grep -i pinger
ab0def6ff888   deployments_pinger   bridge    local

```
## Pipelines

# TASK4

Create pipeline configuration in root director `.gitlab-ci.yml` and enable to auto trigger CICD pipelines for every new commit into any target branch

Created Multi-stage pipeline to have all below stages for different scenarios

1. stage `download_dep` will download all dependencies, validate downloaded dependencies and create artifacts for `vendor` folder
2. stage `build` will build go lang binary, store into ./bin/ folder and create artifact with `pinger` binary
3. stage `test` will perform `go test ./...` and `sast` Static Application Security Testing (SAST)
4. stage `build_image` will build a final docker image, create a tar file, remove existing image, reload image from image tar file and crate artifact for `devops/pinger:latest1` image

build_image will run only for develop and master branches,

All future/hotfix/bugfix branches will be running against `download_dep`, `build` and `test`

There total 3 artifacts are available to download with below names
1. pingerdependencies
2. pingerbinary
3. docker_image_pinger

## Evidence

# pinger services communication

![Feature CICD](./pingercomm.png)


# Feature branch pipeline flows

![Feature CICD](./feature_cicd.png)

# develop and main branch pipeline flows

![develop/main CICD](./develop_main_cicd.png)

# DOCKER Image Build in pipeline

![DockerImageBuild](./dockerbuild.png)


# Artifacts for download

https://gitlab.com/seerla/cognizant-govtech-devops-checkin/-/artifacts

![Artifacts](./artifacts.png)
