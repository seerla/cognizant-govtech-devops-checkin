# Building with latest golang image
#Using Docker Multistage Build strategy to have final image with executable binary


# Created a 3 test cases with exit condition to validate all pre-requisties before creating a final docker image

FROM golang:alpine as builder
WORKDIR /go/src/pinger

COPY go.mod go.sum ./

COPY ./cmd/pinger .

# TEST1: Test for go version
RUN go_version=$(go version | awk '{print $3}' | sed 's/go//g') && \
    if [ "$go_version" '>' "1.20" ]; then \
        echo "Go version is greater than 1.20"; \
    else \
        echo "Go version is not greater than 1.20" && exit 1; \
    fi

RUN go mod vendor

# TEST2: To validate all modules are downloaded
ENV MODULE_VERIFY  "all modules verified"
RUN go_mod_verify=$(go mod verify) && if [ "$go_mod_verify" = "$MODULE_VERIFY" ]; then \
        echo "All go modules are verified"; \
    else \
        echo "Failed to verify all modules" && exit 1; \
    fi


RUN CGO_ENABLED=0 GOOS=linux go build -mod vendor -a -installsuffix cgo -o pinger .

# TEST2: verify pinger file is created

RUN test -e ./pinger && \
    if [ $? -eq 0 ]; then \
     echo "pinger binary is created successfully"; \
    else \
     echo "pinger binary failed to create " && exit 1; \
   fi

## Creating a final slim docker image to run pinger binary

FROM istio/distroless
WORKDIR /app
COPY --from=builder /go/src/pinger/pinger /app/
ENV PATH=$PATH:/app/
ENTRYPOINT ["pinger"]